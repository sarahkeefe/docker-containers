#!/bin/bash

# recon-all -i /input/$(ls /input | sort | head -1) -subjid 0000101_v00_mr -openmp 4 -all

# recon-all #SUBJECT_ID# #RECON_ALL_ARGS# -sd /output -i /input

function die() {
    echo >&2 "$@"
    date >&2
    exit 1
}


inputdir=$1
outputdir=$2
id=$3
label=$4
project=$5
scan_id=$6
t2_ids=$7
flair_ids=$8
shift 8
other_args="$@"


###########
# Start up
echo "Executing recon-all script"
echo "inputdir=${inputdir}"
echo "outputdir=${outputdir}"
echo "id=${id}"
echo "label=${label}"
echo "project=${project}"
echo "scan_id=${scan_id}"
echo "t2_ids=${t2_ids}"
echo "flair_ids=${flair_ids}"
echo "other_args=${other_args}"

inputfile=$(ls $inputdir | sort | head -1)
[[ $inputfile == "" ]] && die "Could not find an input file in input directory $inputdir"
echo "inputfile=${inputfile}"

###########
# Recon-all
echo
echo "Starting recon-all"
date

# Construct recon-all args from params
recon_all_args_combined="-s $label -sd $outputdir $recon_all_args"

for t1 in ${scan_id[*]}; do
    scan_file=`ls ${inputdir}/$t1 | head -1`
    # if [[ -z $scan_file ]]; then
    #     die "Could not find a file in directory ${inputdir}/$t1"
    # fi
    recon_all_args_combined="$recon_all_args_combined -i ${inputdir}/${t1}/$scan_file"
done

for t2 in ${t2_ids[*]}; do
    scan_file=`ls ${inputdir}/$t2 | head -1`
    # if [[ -z $scan_file ]]; then
    #     die "Could not find a file in directory ${inputdir}/$t2"
    # fi
    recon_all_args_combined="$recon_all_args_combined -T2 ${inputdir}/${t2}/$scan_file"
done

# Add the T2pial flag if we have T2 IDs being included
if [ "${t2_ids}" != "" ] && [ ${#t2_ids[@]} -gt 0 ]; then
    recon_all_args_combined="$recon_all_args_combined -T2pial"
fi

for flair in ${flair_ids[*]}; do
    scan_file=`ls ${inputdir}/$flair | head -1`
    # if [[ -z $scan_file ]]; then
    #     die "Could not find a file in directory ${inputdir}/$flair"
    # fi
    recon_all_args_combined="$recon_all_args_combined -FLAIR ${inputdir}/${flair}/$scan_file"
done

# Add the FLAIRpial flag if we have FLAIR IDs being included
if [ "${flair_ids}" != "" ] && [ ${#flair_ids[@]} -gt 0 ]; then
    recon_all_args_combined="$recon_all_args_combined -FLAIRpial"
fi


cmd="recon-all ${recon_all_args_combined} ${other_args}"
echo ${cmd}
eval ${cmd} || die "Recon-all failed"

echo "Finished recon-all"
date

###########
# Generate assessor XML
assessor_id="${id}_freesurfer_$(date +%Y%m%d%H%M%S)"

echo
echo "Generating assessor XML"
date

flair_t2_params=""
if [ "${flair_ids}" != "" ] && [ ${#flair_ids[@]} -gt 0 ]; then
    flair_t2_params="$flair_t2_params -l ${flair_ids}"
fi

if [ "${t2_ids}" != "" ] && [ ${#t2_ids[@]} -gt 0 ]; then
    flair_t2_params="$flair_t2_params -o ${t2_ids}"
fi

cmd="usr/local/bin/stats2xml_fs6.0.pl -p ${project} -x ${id} -t Freesurfer -f ${assessor_id} -m ${scan_id} ${flair_t2_params} -a "'"'${other_args}'"'" /output/${label}/stats"

echo ${cmd}
eval ${cmd} || die "Failed to generate assessor XML"

cmd="cp *.xml /output/"
echo ${cmd}
eval ${cmd} || die "Failed to copy assessor XML"

echo "Finished generating assessor XML"

echo "Resolve up any symlinks in output folder"
cmd="rm -f /output/rh.EC_average"
echo ${cmd}
eval ${cmd} 
cmd="mv /freesurfer60/freesurfer/subjects/rh.EC_average /output/"
echo ${cmd}
eval ${cmd} 

cmd="rm -f /output/lh.EC_average"
echo ${cmd}
eval ${cmd} 
cmd="mv /freesurfer60/freesurfer/subjects/lh.EC_average /output/"
echo ${cmd}
eval ${cmd} 

cmd="rm -f /output/fsaverage"
echo ${cmd}
eval ${cmd} 
cmd="mv /freesurfer60/freesurfer/subjects/fsaverage /output/"
echo ${cmd}
eval ${cmd} 


date

