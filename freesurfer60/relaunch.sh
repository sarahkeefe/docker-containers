#!/bin/bash

# recon-all -i /input/$(ls /input | sort | head -1) -subjid 0000101_v00_mr -openmp 4 -all

# recon-all #SUBJECT_ID# #RECON_ALL_ARGS# -sd /output -i /input

function die() {
    echo >&2 "$@"
    date >&2
    exit 1
}


corrected_freesurfer=$1
outputdir=$2
id=$3
label=$4
project=$5
scan_id=$6
t2_ids=$7
flair_ids=$8
shift 8
other_args="$@"


###########
# Start up
echo "Executing recon-all script"
echo "corrected_freesurfer=${corrected_freesurfer}"
echo "outputdir=${outputdir}"
echo "id=${id}"
echo "label=${label}"
echo "project=${project}"
echo "scan_id=${scan_id}"
echo "t2_ids=${t2_ids}"
echo "flair_ids=${flair_ids}"
echo "other_args=${other_args}"

inputfile=$(ls $corrected_freesurfer | sort | head -1)
[[ $inputfile == "" ]] && die "Could not find an input file in input directory $corrected_freesurfer"
echo "inputfile=${inputfile}"

##################
# Copy 
echo "Copying corrected Freesurfer files"
cmd="cp -r -p ${corrected_freesurfer}/${label} ${outputdir}/"
echo ${cmd}
${cmd} || die "Corrected Freesurfer data copy fail"

###########
# Recon-all
echo
echo "Starting recon-all relaunch"
date
cmd="recon-all -sd ${outputdir} -subjid ${label} ${other_args}"
echo ${cmd}
${cmd} || die "Recon-all failed"

echo "Finished recon-all relaunch"
date

###########
# Generate assessor XML
assessor_id="${id}_freesurfer_$(date +%Y%m%d%H%M%S)"

echo
echo "Generating assessor XML for"
echo "assessor_id="${assessor_id}
date

#cmd='stats2xml_fs5.3.pl -p ${project} -x ${id} -t Freesurfer -f ${assessor_id} -m ${scan_id} -a "${other_args}" -r -m ${scan_id} /output/${label}/stats'
cmd="stats2xml_fs6.0.pl -p ${project} -x ${id} -t Freesurfer -f ${assessor_id} -r -n "'"'${other_args}'"'" ${outputdir}/${label}/stats"

echo ${cmd}

${cmd} || die "Failed to generate assessor XML"
popd
echo "Finished generating assessor XML"

echo "Resolve up any symlinks in output folder"
cmd="rm -f /output/rh.EC_average"
echo ${cmd}
eval ${cmd} 
cmd="mv /freesurfer60/freesurfer/subjects/rh.EC_average /output/"
echo ${cmd}
eval ${cmd} 

cmd="rm -f /output/lh.EC_average"
echo ${cmd}
eval ${cmd} 
cmd="mv /freesurfer60/freesurfer/subjects/lh.EC_average /output/"
echo ${cmd}
eval ${cmd} 

cmd="rm -f /output/fsaverage"
echo ${cmd}
eval ${cmd} 
cmd="mv /freesurfer60/freesurfer/subjects/fsaverage /output/"
echo ${cmd}
eval ${cmd} 

date
