#!/bin/bash
me=$0
n=$#
USAGE="
USAGE: $me <inputdir> <outputdir> <session_label>
       This script creates the snapshot images of the aparc+aseg, brainmask, and T1 volumes.
"
    
if [ "$n" == "0" ] 
then
 echo $USAGE
 exit 1
fi

inputdir=$1
outputdir=$2
session_label=$3

###########
# Start up
echo "Executing snapshots script"
echo "inputdir=${inputdir}"
echo "outputdir=${outputdir}"
echo "session_label=${session_label}"

###########
# Snapshot generate
echo
echo "Starting snapshot generation"
date

subject=$session_label

cp -r $inputdir/* $outputdir/.

export SUBJECTS_DIR=$outputdir
path=$SUBJECTS_DIR
spath=$SUBJECTS_DIR/$subject

export FREESURFER_HOME=/freesurfer53/freesurfer


################################
# BRAINMASK SNAPSHOTS CMD SETUP
################################

# start brainmask command create
# axial
touch ${path}/brnmsk_axl_snapshots_command.txt

echo "-v ${spath}/mri/brainmask.mgz:grayscale=0,175" > ${path}/brnmsk_axl_snapshots_command.txt

firstslice=47
lastslice=200
i=$firstslice

while [ $i -lt $lastslice ]; do
    i_string=`printf "%03d" $i`    
    echo " --viewport axial -zoom 1 -cam azimuth 180 elevation -90 --viewsize 600 600 -slice $i $i $i -ss ${spath}/snapshots/${subject}_brnmsk_axl_${i_string}.png" >> ${path}/brnmsk_axl_snapshots_command.txt
    i=$(( $i + 2))
done

echo "-quit" >> ${path}/brnmsk_axl_snapshots_command.txt
# end axial


# coronal
touch ${path}/brnmsk_cor_snapshots_command.txt

echo "-v ${spath}/mri/brainmask.mgz:grayscale=0,175" >> ${path}/brnmsk_cor_snapshots_command.txt

firstslice=33
lastslice=212
i=$firstslice

while [ $i -lt $lastslice ]; do
    i_string=`printf "%03d" $i`    
    echo "--viewport coronal -zoom 1 -cam azimuth 180 elevation -90 --viewsize 600 600 -slice $i $i $i -ss ${spath}/snapshots/${subject}_brnmsk_cor_${i_string}.png" >> ${path}/brnmsk_cor_snapshots_command.txt
    i=$(( $i + 2))
done

echo "-quit" >> ${path}/brnmsk_cor_snapshots_command.txt
# end coronal


# sagittal
touch ${path}/brnmsk_sag_snapshots_command.txt

echo "-v ${spath}/mri/brainmask.mgz:grayscale=0,175" > ${path}/brnmsk_sag_snapshots_command.txt

firstslice=63
lastslice=194
i=$firstslice

while [ $i -lt $lastslice ]; do
    i_string=`printf "%03d" $i`    
    echo "--viewport sagittal -zoom 1 -cam azimuth 180 elevation -90 --viewsize 600 600 -slice $i $i $i -ss ${spath}/snapshots/${subject}_brnmsk_sag_${i_string}.png" >> ${path}/brnmsk_sag_snapshots_command.txt
    i=$(( $i + 2))
done

echo "-quit" >> ${path}/brnmsk_sag_snapshots_command.txt
# end coronal
# end brainmask



###################################
# T1/BOUNDARIES SNAPSHOTS CMD SETUP
###################################

# start t1 command create
# axial
touch ${path}/t1_axl_snapshots_command.txt

if [ -e "$spath/mri/T1.mgz" ]; then
    echo "-v ${spath}/mri/T1.mgz:grayscale=0,175 -f ${spath}/surf/lh.white:edgecolor=255,255,0:edgethickness=1 ${spath}/surf/lh.pial:edgecolor=red:edgethickness=1 ${spath}/surf/rh.white:edgecolor=255,255,0:edgethickness=1 ${spath}/surf/rh.pial:edgecolor=red:edgethickness=1" > ${path}/t1_axl_snapshots_command.txt
fi

if [ -e "$spath/mri/t1.mgz" ]; then
    echo "-v ${spath}/mri/t1.mgz:grayscale=0,175 -f ${spath}/surf/lh.white:edgecolor=255,255,0:edgethickness=1 ${spath}/surf/lh.pial:edgecolor=red:edgethickness=1 ${spath}/surf/rh.white:edgecolor=255,255,0:edgethickness=1 ${spath}/surf/rh.pial:edgecolor=red:edgethickness=1" > ${path}/t1_axl_snapshots_command.txt
fi

firstslice=47
lastslice=200
i=$firstslice

while [ $i -lt $lastslice ]; do
    i_string=`printf "%03d" $i`    
    echo "--viewport axial -zoom 1 -cam azimuth 180 elevation -90 --viewsize 600 600 -slice $i $i $i -ss ${spath}/snapshots/${subject}_t1_axl_${i_string}.png" >> ${path}/t1_axl_snapshots_command.txt
    i=$(( $i + 2))    
done

echo "-quit" >> ${path}/t1_axl_snapshots_command.txt
# end axial


# coronal
touch ${path}/t1_cor_snapshots_command.txt

if [ -e "$spath/mri/T1.mgz" ]; then
    echo "-v ${spath}/mri/T1.mgz:grayscale=0,175 -f ${spath}/surf/lh.white:edgecolor=255,255,0:edgethickness=1 ${spath}/surf/lh.pial:edgecolor=red:edgethickness=1 ${spath}/surf/rh.white:edgecolor=255,255,0:edgethickness=1 ${spath}/surf/rh.pial:edgecolor=red:edgethickness=1" > ${path}/t1_cor_snapshots_command.txt
fi

if [ -e "$spath/mri/t1.mgz" ]; then
    echo "-v ${spath}/mri/t1.mgz:grayscale=0,175 -f ${spath}/surf/lh.white:edgecolor=255,255,0:edgethickness=1 ${spath}/surf/lh.pial:edgecolor=red:edgethickness=1 ${spath}/surf/rh.white:edgecolor=255,255,0:edgethickness=1 ${spath}/surf/rh.pial:edgecolor=red:edgethickness=1" > ${path}/t1_cor_snapshots_command.txt
fi

firstslice=33
lastslice=212
i=$firstslice

while [ $i -lt $lastslice ]; do
    i_string=`printf "%03d" $i`    
    echo "--viewport coronal -zoom 1 -cam azimuth 180 elevation -90 --viewsize 600 600 -slice $i $i $i -ss ${spath}/snapshots/${subject}_t1_cor_${i_string}.png" >> ${path}/t1_cor_snapshots_command.txt
    i=$(( $i + 2))
done

echo "-quit" >> ${path}/t1_cor_snapshots_command.txt
# end coronal


# sagittal
touch ${path}/t1_sag_snapshots_command.txt

if [ -e "$spath/mri/T1.mgz" ]; then
    echo "-v ${spath}/mri/T1.mgz:grayscale=0,175 -f ${spath}/surf/lh.white:edgecolor=255,255,0:edgethickness=1 ${spath}/surf/lh.pial:edgecolor=red:edgethickness=1 ${spath}/surf/rh.white:edgecolor=255,255,0:edgethickness=1 ${spath}/surf/rh.pial:edgecolor=red:edgethickness=1" > ${path}/t1_sag_snapshots_command.txt
fi

if [ -e "$spath/mri/t1.mgz" ]; then
    echo "-v ${spath}/mri/t1.mgz:grayscale=0,175 -f ${spath}/surf/lh.white:edgecolor=255,255,0:edgethickness=1 ${spath}/surf/lh.pial:edgecolor=red:edgethickness=1 ${spath}/surf/rh.white:edgecolor=255,255,0:edgethickness=1 ${spath}/surf/rh.pial:edgecolor=red:edgethickness=1" > ${path}/t1_sag_snapshots_command.txt
fi

firstslice=63
lastslice=194
i=$firstslice

while [ $i -lt $lastslice ]; do
    i_string=`printf "%03d" $i`    
    echo "--viewport sagittal -zoom 1 -cam azimuth 180 elevation -90 --viewsize 600 600 -slice $i $i $i -ss ${spath}/snapshots/${subject}_t1_sag_${i_string}.png" >> ${path}/t1_sag_snapshots_command.txt
    i=$(( $i + 2))
done

echo "-quit" >> ${path}/t1_sag_snapshots_command.txt
# end sagittal
# end t1



################################
# APARC+ASEG SNAPSHOTS CMD SETUP
################################

# start aparcaseg command create
# axial
touch ${path}/aparc_aseg_axl_snapshots_command.txt

if [ -e "$spath/mri/T1.mgz" ]; then
    echo "-v ${spath}/mri/T1.mgz:grayscale=0,175 ${spath}/mri/aparc+aseg.mgz:colormap=lut:opacity=0.3" > ${path}/aparc_aseg_axl_snapshots_command.txt
fi

if [ -e "$spath/mri/t1.mgz" ]; then
    echo "-v ${spath}/mri/t1.mgz:grayscale=0,175 ${spath}/mri/aparc+aseg.mgz:colormap=lut:opacity=0.3" > ${path}/aparc_aseg_axl_snapshots_command.txt
fi

firstslice=47
lastslice=200
i=$firstslice

while [ $i -lt $lastslice ]; do
    i_string=`printf "%03d" $i`    
    echo "--viewport axial -zoom 1 -cam azimuth 180 elevation -90 --viewsize 600 600 -slice $i $i $i -ss ${spath}/snapshots/${subject}_aparc_aseg_axl_${i_string}.png" >> ${path}/aparc_aseg_axl_snapshots_command.txt
    i=$(( $i + 2))
done

echo "-quit" >> ${path}/aparc_aseg_axl_snapshots_command.txt
# end axial


# coronal
touch ${path}/aparc_aseg_cor_snapshots_command.txt

if [ -e "$spath/mri/T1.mgz" ]; then
    echo "-v ${spath}/mri/T1.mgz:grayscale=0,175 ${spath}/mri/aparc+aseg.mgz:colormap=lut:opacity=0.3" > ${path}/aparc_aseg_cor_snapshots_command.txt
fi

if [ -e "$spath/mri/t1.mgz" ]; then
    echo "-v ${spath}/mri/t1.mgz:grayscale=0,175 ${spath}/mri/aparc+aseg.mgz:colormap=lut:opacity=0.3" > ${path}/aparc_aseg_cor_snapshots_command.txt
fi

firstslice=33
lastslice=212
i=$firstslice

while [ $i -lt $lastslice ]; do
    i_string=`printf "%03d" $i`    
    echo "--viewport coronal -zoom 1 -cam azimuth 180 elevation -90 --viewsize 600 600 -slice $i $i $i -ss ${spath}/snapshots/${subject}_aparc_aseg_cor_${i_string}.png" >> ${path}/aparc_aseg_cor_snapshots_command.txt
    i=$(( $i + 2))
done

echo "-quit" >> ${path}/aparc_aseg_cor_snapshots_command.txt
# end coronal


# sagittal
touch ${path}/aparc_aseg_sag_snapshots_command.txt

if [ -e "$spath/mri/T1.mgz" ]; then
    echo "-v ${spath}/mri/T1.mgz:grayscale=0,175 ${spath}/mri/aparc+aseg.mgz:colormap=lut:opacity=0.3" > ${path}/aparc_aseg_sag_snapshots_command.txt
fi

if [ -e "$spath/mri/t1.mgz" ]; then
    echo "-v ${spath}/mri/t1.mgz:grayscale=0,175 ${spath}/mri/aparc+aseg.mgz:colormap=lut:opacity=0.3" > ${path}/aparc_aseg_sag_snapshots_command.txt
fi

firstslice=63
lastslice=194
i=$firstslice

while [ $i -lt $lastslice ]; do
    i_string=`printf "%03d" $i`
    echo "--viewport sagittal -zoom 1 -cam azimuth 180 elevation -90 --viewsize 600 600 -slice $i $i $i -ss ${spath}/snapshots/${subject}_aparc_aseg_sag_${i_string}.png" >> ${path}/aparc_aseg_sag_snapshots_command.txt
    i=$(( $i + 2))
done

echo "-quit" >> ${path}/aparc_aseg_sag_snapshots_command.txt
# end sagittal
# end aparcaseg



################################
# ACTUAL SNAPSHOTS COMMAND CALLS
################################

if [ ! -e "$spath/snapshots" ]
then
  mkdir -p $spath/snapshots
fi

echo "calling snapshop commands"

cat ${path}/t1_axl_snapshots_command.txt
cat ${path}/t1_cor_snapshots_command.txt
cat ${path}/t1_sag_snapshots_command.txt
cat ${path}/brnmsk_axl_snapshots_command.txt
cat ${path}/brnmsk_cor_snapshots_command.txt
cat ${path}/brnmsk_sag_snapshots_command.txt
cat ${path}/aparc_aseg_axl_snapshots_command.txt
cat ${path}/aparc_aseg_cor_snapshots_command.txt
cat ${path}/aparc_aseg_sag_snapshots_command.txt

# T1 boundaries
freeview -cmd ${path}/t1_axl_snapshots_command.txt
freeview -cmd ${path}/t1_cor_snapshots_command.txt
freeview -cmd ${path}/t1_sag_snapshots_command.txt

# Brainmask
freeview -cmd ${path}/brnmsk_axl_snapshots_command.txt
freeview -cmd ${path}/brnmsk_cor_snapshots_command.txt
freeview -cmd ${path}/brnmsk_sag_snapshots_command.txt

# aparc+aseg
freeview -cmd ${path}/aparc_aseg_axl_snapshots_command.txt
freeview -cmd ${path}/aparc_aseg_cor_snapshots_command.txt
freeview -cmd ${path}/aparc_aseg_sag_snapshots_command.txt


##############################
# SNAPSHOTS CONVERT PNG TO GIF
##############################

# Convert to gif and delete the png files
pushd $spath/snapshots
if [ `ls | egrep -c "\.png"` ]
    then
        for f in *.png; do 
            png_file_base=${f%%.*}
            convert -scale 300x300 $f ${png_file_base}.gif
            rm $f
        done 
    fi
popd

echo "$me all done"
