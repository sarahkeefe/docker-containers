# PUP Docker Image

##Container folder contents
- Dockerfile
- .license (FreeSurfer licence file)

##Example command:
`docker run -v /path/to/input/folder:/input -v /path/to/output/folder:/output -t pup petproc /input/SESSIONLABEL.params`