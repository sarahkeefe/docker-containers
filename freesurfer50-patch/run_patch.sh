#!/bin/bash
#
# Run FS5.1 patch on v. 5.0 Freesurfers
# For more details see https://wunic.wustl.edu/cnda-help/freesurfer-patch-10dec2012-3441330.html
#


# usage instructions
if [ ${#@} == 0 ]; then
    echo ""
    echo "Freesurfer 5.1 patch for FreeSurfer version 5.0"
    echo ""
    echo "Run FS5.1 patch on v. 5.0 Freesurfers"
    echo "For more details see https://wunic.wustl.edu/cnda-help/freesurfer-patch-10dec2012-3441330.html"
    echo ""   
    echo "Usage: $0 input_file.csv base_directory_name"
    echo "<input_file>: A Unix formatted, comma separated file containing the following columns:"
    echo "    freesurfer_id (MUST be the ID that starts with the accession number, i.e. CNDA_E12345_freesurfer_0123456789)"
    echo "    mr session id"
    echo "    mr session label"
    echo "    included t1 id"
    echo "    FS QC status"
    echo "    FS QC Method"
    echo "    FS QC Date"
    echo "    FS QC Notes"
    echo "    FS QC Validated By"
    echo "    FS Version"
    echo "    FS Notes (existing notes from initial upload)"
    echo "    FS Notes Addendum (for adding to existing notes)"
    echo "<directory_name>: SUBJECTS_DIR for Freesurfer (where all your FS files are)"    
    echo "<output_dirname>: output folder to save zips and XMLs to"    
else 

    # Get the input arguments
    INFILE=$1
    DIRNAME=$2
    OUTPUTDIR=$3

    mkdir -p ${OUTPUTDIR}/patched_fs
    mkdir -p ${OUTPUTDIR}/patched_zips
    mkdir -p ${OUTPUTDIR}/backup_zips
    mkdir -p ${OUTPUTDIR}/fixed_xmls

	export FREESURFER_HOME=/freesurfer51/freesurfer-Linux-centos4_x86_64-stable-pub-v5.1.0
	export SUBJECTS_DIR=${OUTPUTDIR}/patched_fs
	cd ${SUBJECTS_DIR}
	source ${FREESURFER_HOME}/SetUpFreeSurfer.sh

    cat $INFILE | while IFS=, read -r FS_ID PROJECT_ID MR_ID MR_LABEL T1_ID QC_STATUS QC_METHOD QC_DATE QC_NOTES QC_PERSON FS_NOTES; do

    	echo "Getting started with ${MR_ID} (FS ID ${FS_ID})."

    	echo "Copying the input Freesurfer to ${OUTPUTDIR}/patched_fs."

    	cp -R ${DIRNAME}/${MR_LABEL} ${OUTPUTDIR}/patched_fs/. 

    	echo "Zipping the old (invalid) files to keep as a backup."

		zip -r ${OUTPUTDIR}/backup_zips/${FS_ID}_invalidfilesbackup.zip ${MR_LABEL}/scripts ${MR_LABEL}/stats/aseg.stats ${MR_LABEL}/stats/wmparc.stats ${MR_LABEL}/mri/transforms/talairach_avi.log ${MR_LABEL}/mri/transforms/talairach.xfm ${MR_LABEL}/mri/transforms/talsrcimg_to_711-2C_as_mni_average_305_t4_vox2vox.txt ${MR_LABEL}/mri/wmparc.mgz

		echo "Changing directory to ${SUBJECTS_DIR}/${MR_LABEL}/mri"

		cd ${SUBJECTS_DIR}/${MR_LABEL}/mri

		echo "Running talairach_avi --i nu.mgz --xfm transforms/talairach.xfm"
		talairach_avi --i nu.mgz --xfm transforms/talairach.xfm
		echo "Done with talaraich_avi step"
		echo "Running recon-all -s ${MR_LABEL} -segstats"
		recon-all -s ${MR_LABEL} -segstats
		echo "Running recon-all -s ${MR_LABEL} -wmparc"
		echo Running recon-all -s ${MR_LABEL} -wmparc
		recon-all -s ${MR_LABEL} -wmparc

		echo "Going back to directory ${SUBJECTS_DIR} before zipping."
		cd ${SUBJECTS_DIR}

		echo "Done with recon-all steps for ${MR_LABEL} - zipping the results."

		# Zip corrected files
		zip -r ${OUTPUTDIR}/patched_zips/${FS_ID}_correctedfiles.zip ${MR_LABEL}/scripts ${MR_LABEL}/stats/aseg.stats ${MR_LABEL}/stats/wmparc.stats ${MR_LABEL}/mri/transforms/talairach_avi.log ${MR_LABEL}/mri/transforms/talairach.xfm ${MR_LABEL}/mri/transforms/talsrcimg_to_711-2C_as_mni_average_305_t4_vox2vox.txt ${MR_LABEL}/mri/wmparc.mgz			

		echo "Done zipping."

		qc_date_mo=`echo $QC_DATE | cut -d'/' -f1 | awk '{ printf "%02d", $1 }'`
		qc_date_day=`echo $QC_DATE | cut -d'/' -f2 | awk '{ printf "%02d", $1 }'`
		qc_date_yr=`echo $QC_DATE | cut -d'/' -f1 | awk '{ printf "%04d", $1 }'`

		echo "Generating updated XML."

	    /usr/local/bin/stats2xml_fs5.0_for_upload.pl -p $PROJECT_ID -x $MR_ID -t Freesurfer -f $FS_ID -m $T1_ID -c "${QC_STATUS}" ${SUBJECTS_DIR}/${MR_LABEL}/stats
	    
	    echo "Generated XML for ${MR_LABEL}"

	    updated_details_note=$( echo "$FS_NOTES" | sed 's/\//\\\//g' )
	    updated_qc_notes=$( echo "$QC_NOTES" | sed 's/\//\\\//g' )
	    updated_qc_validated_by=$( echo "$QC_PERSON" | sed 's/\//\\\//g' )
	    updated_qc_method=$( echo "$QC_METHOD" | sed 's/\//\\\//g' )
	    updated_qc_date=${qc_date_yr}-${qc_date_mo}-${qc_date_day}

	    # replace placeholder fields in the generated XML
	    find ${MR_ID}_freesurfer5.xml -type f -exec sed -i "s/REPLACE_WITH_FREESURFER_DETAILS_NOTE/${updated_details_note}/g" {} \;
	    find ${MR_ID}_freesurfer5.xml -type f -exec sed -i "s/REPLACE_WITH_FREESURFER_QC_NOTES/${updated_qc_notes}/g" {} \;
	    find ${MR_ID}_freesurfer5.xml -type f -exec sed -i "s/REPLACE_WITH_FREESURFER_QC_VALIDATED_BY/${updated_qc_validated_by}/g" {} \;
	    find ${MR_ID}_freesurfer5.xml -type f -exec sed -i "s/REPLACE_WITH_FREESURFER_QC_METHOD/${updated_qc_method}/g" {} \;
	    find ${MR_ID}_freesurfer5.xml -type f -exec sed -i "s/REPLACE_WITH_FREESURFER_QC_DATE/${updated_qc_date}/g" {} \;

	    echo "Added in correct QC notes for ${FS_ID}"

	    mv ${MR_ID}_freesurfer5.xml ${OUTPUTDIR}/fixed_xmls

		echo "Moved the patched XML file to ${OUTPUTDIR}/fixed_xmls"

	    echo "Done patching, zipping, and creating XML for ${MR_LABEL} (FS ID ${FS_ID})" 

	done
fi